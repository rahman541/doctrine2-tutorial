## Doctrine v2 Tutorial
Link ref: http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html

## Usage
```
composer install
php create_product.php "Test Product Name"
php list_products.php
php show_product.php 1
php update_product.php 1 "Tst1"
```